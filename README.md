# First-class instances

Type classes are data types. This is just how type classes are compiled
by GHC. This package makes that correspondence explicit as a proper language
feature, with a simple but powerful mechanism to define instances. This enables
implementations of *deriving* as libraries.

## Type classes as data types

Type classes are associated to data types using the `mkDict` command.

Example:

```haskell
class Monoid a where
  mempty :: a
  (<>) :: a -> a -> a

mkDict ''Monoid
```

That generates the following data type:

```haskell
data DictMonoid a = Monoid
  { _mempty :: a
  , (|<>) :: a -> a -> a
  }
```

## Dictionaries

The `Dict` type family maps constraints to their corresponding dictionary types.

```haskell
type family Dict (c :: Constraint) :: Type    -- Defined in FCI

type instance Dict (Monoid a) = DictMonoid a  -- also from mkDict ''Monoid
```

A constraint `c` can be reflected as a dictionary:

```haskell
dict @c :: c => Dict c
```

## Instances

A dictionary `d` can be reified as an instance.

Example:

```haskell
instanceDict [| d :: Dict (Monoid T) |]
```

That generates the following instance:

```haskell
instance Monoid T where
  mempty = _mempty d
  (<>) = (|<>) d
```

Those are the core features provided by *first-class-instances*.

## Show me the types

```haskell
{- module FCI -}

type Dict :: Constraint -> Type

dict :: forall c. c => Dict c

mkDict :: Name -> Q [Dec]

instanceDict :: Q Type -> Q [Dec]


{- module FCI.Unsafe -}

(==>) :: forall c r. Dict c -> (c => r) -> r
```

## Deriving as a library

These simple primitives can be used to implement "deriving as a library".
The adventure continues in [*ad-lib*](https://gitlab.com/lysxia/ad-lib).

## Related work

- The [*reflection*](https://hackage.haskell.org/package/reflection) library
  is another approach to convert constraints into first-class values,
  and back. It provides a solution for implicit configuration, rather than
  deriving type class instances for user-defined types.

- In Agda,
  [all data types are type classes](https://agda.readthedocs.io/en/latest/language/instance-arguments.html),
  exemplifying the orthogonality of declaring a data type, and passing values
  implicitly as instances.
  Haskell requires instances to be globally unique: there must not be two instances
  `Monoid T` for the same `T`.
