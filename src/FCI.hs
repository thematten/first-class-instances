{-# LANGUAGE
  KindSignatures #-}

-- |
-- = First-class instances
--
-- Type classes are data types.
--
-- For example, this type class:
--
-- @
-- class Eq a where
--   (==) :: a -> a -> Bool
-- @
--
-- is compiled to this data type by GHC (a dictionary type):
--
-- @
-- data DictEq a = Eq
--  { (|==) :: a -> a -> Bool }
-- @
--
-- This library makes that correspondence explicit.
--
-- == Overview
--
-- Every constraint @c@ is associated to a dictionary type @Dict c@.
--
-- Examples:
--
-- @
-- 'FCI.Dict' 'Eq' = 'FCI.Base.DictEq'
-- 'FCI.Dict' 'Functor' = 'FCI.Base.DictFunctor'
-- @
--
-- == Generate dictionary types
--
-- Dictionary types must first be explicitly defined for each class.
--
-- @
-- 'mkDict' ''Eq
--
-- -- expands to --
--
-- data DictEq a = Eq
--   { (|==) :: a -> a -> Bool }
--
-- type instance 'Dict' (Eq a) = DictEq a
-- @
--
-- == Declare top-level instances from dictionaries
--
-- An instance @Eq T@ can now be declared using a dictionary @d :: Dict (Eq T)@
-- (@d@ can be any expression).
--
-- @
-- 'instanceDict' [| d :: Dict (Eq T) |]
-- @
--
-- == Reflect a constraint as a dictionary
--
-- Any constraint can be turned into a dictionary, manipulating it as a value.
--
-- @
-- dict \@c :: c => Dict c
-- @
--
-- The inverse, reifying a dictionary into a constraint, would break coherence.
-- Hence, it's quarantined away in "FCI.Unsafe".
module FCI (
    -- * API
    Dict

    -- ** Generate dictionary types
  , mkDict
  , DictOptions
  , methodName, superclassName, typeName, constructorName, autoDoc
  , defaultDictOptions
  , setDictOptions

    -- ** Reflect dictionaries
  , dict

    -- ** Reify dictionaries
  , instanceDict
  , instanceDict_
  , Overlap(Overlappable, Overlapping, Overlaps, Incoherent)
  ) where

import Data.Kind (Type, Constraint)
import FCI.Internal (dict)
import qualified FCI.Internal as Internal
import FCI.TH

-------------------------------------------------------------------------------
-- | Translation @Constraint -> Type@.
-- The underlying type family is hidden so that 'FCI.mkDict'
-- is the only way to extend it, maintaining invariants required by 'dict'.
--
-- For example:
--
-- @
-- class Bar a => Foo a where
--   baz :: a
--   qux :: a -> b -> [(a, b)]
--
-- 'FCI.mkDict' 'Foo
-- @
--
-- creates the following declarations:
--
-- @
-- type instance Dict (Foo a) = DictFoo a
-- data DictFoo a = Foo{
--     _Bar :: Dict (Bar a)
--   , baz  :: a
--   , qux  :: forall b. a -> b -> [(a, b)]
--   }
-- @
type Dict (c :: Constraint) = Internal.Dict c :: Type
