{-# LANGUAGE
  ConstraintKinds,
  GADTs,
  KindSignatures,
  RankNTypes,
  ScopedTypeVariables,
  TypeApplications,
  TypeFamilies,
  TypeFamilyDependencies,
  TypeOperators #-}

-- | Core types. This module is internal and provides no guarantees about
-- stability and safety of its interface.
module FCI.Internal
  ( Dict
  , dict
  , (==>)
  ) where

import Data.Kind (Type, Constraint)
import Unsafe.Coerce (unsafeCoerce)

-- $setup
-- >>> import Data.Coerce
-- >>> import FCI.Base

-------------------------------------------------------------------------------
-- | Type family that maps constraint to its first class representation -
-- should be generally used instead of its concrete result for consistency.
type family Dict (c :: Constraint) = (t :: Type) | t -> c
-- TODO: should be TC plugin? - tuples, QuantifiedConstraints etc.

-------------------------------------------------------------------------------
infixr 0 ==>
infixr 1 :=>

-------------------------------------------------------------------------------
-- | /Reflect/ a constraint as a dictionary value.
--
-- You can use @TypeApplications@ to make the constraint @c@ explicit.
-- 'Dict' is injective, so @c@ may be inferred sometimes.
dict :: forall c. c => Dict c
dict = case unsafeCoerce id :: c :=> Dict c of Wants d -> d

-------------------------------------------------------------------------------
-- | /Reify/ a dictionary to resolve a constraint required by the second operand.
--
-- For example, this lets us use @+@ on a type that doesn't have a top-level
-- 'Num' instance.
--
-- >>> newtype Foo = Foo Int deriving Show
-- >>> (coerce (dict @(Num Int)) :: Dict (Num Foo)) ==> Foo 1 + Foo 2
-- Foo 3
(==>) :: forall c r. Dict c -> (c => r) -> r
d ==> x = unsafeCoerce (Wants @c @r x) d

-------------------------------------------------------------------------------
-- | Wrapper for value @r@ requiring constraint @c@. Used by 'dict' and ('==>')
-- to satisfy typechecker.
newtype c :=> r where
  Wants :: (c => r) -> c :=> r
