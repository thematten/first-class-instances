-- | Reify first-class instances as constraints.
-- 
-- This breaks the global property that there is at most one instance
-- for each constraint. For example, sets and maps in containers rely
-- on that property to maintain their invariants.
--
-- Use at your own risk.
module FCI.Unsafe 
  ( (==>)
  ) where

import FCI.Internal ((==>))
