{-# LANGUAGE
  BlockArguments,
  CPP,
  LambdaCase,
  MultiWayIf,
  NamedFieldPuns,
  PatternSynonyms,
  RankNTypes,
  ScopedTypeVariables,
  TemplateHaskell,
  ViewPatterns #-}

-- | TH for dictionary representation generation. This module is internal and
-- provides no guarantees about stability and safety of its interface.
module FCI.TH (
    -- * Main API
    mkDict
  , setDictOptions
  , DictOptions(methodName,superclassName,typeName,constructorName,autoDoc)
  , defaultDictOptions
  , instanceDict
  , instanceDict_
  , TH.Overlap(Overlappable, Overlapping, Overlaps, Incoherent)
    -- * Synonyms
  , ClassName
  , MethodName
  , FieldName
  , TypeName
  , ConstrName
  ) where

import Language.Haskell.TH.Ppr (pprint)
import Language.Haskell.TH.Syntax as TH

import Control.Monad (when)
import Control.Monad.Trans.State
import           Data.Char           (isAlpha)
import Data.Data
import qualified Data.Kind as K
import           Data.List           (foldl1', stripPrefix)
import qualified Data.Map.Strict as M
import           Data.Maybe          (fromMaybe, mapMaybe)

import FCI.Internal (Dict)

-------------------------------------------------------------------------------
-- | Declare the dictionary type associated with a given class.
-- The generated type is a record of class members with the following format
-- by default (it can be customized using 'setDictOptions'):
--
-- * The type of the record is `Dict` prepended to the name of the class.
--
-- * The name of the constructor is the name of the class.
--
-- * Superclass constraints are transformed into fields containing their
--   dictionaries. The names of those fields are generated this way:
--
--     * Alphabetic names ('Show', 'Applicative') are prefixed with @_@
--     * Operators (('~')) are prefixed with @/@
--     * Tuples are converted into @_Tuple2@, @_Tuple3@, etc.
--     * Multiple occurencies of the same superclass are suffixed with an index
--       starting from 1, or with an increasing number of @|@s if its name is
--       an operator.
--
-- * Methods get their own fields; their names are the names of methods
--   prefixed with @_@ for alphabetic method names, or @|@ for operators.
mkDict :: Name -> Q [Dec]
mkDict cname = do
  opts <- getDictOptions
  mkDictWith opts cname

type ClassName = String
type MethodName = String
type FieldName = String
type TypeName = String
type ConstrName = String

-- | Options to configure 'mkDict'. The constructor is hidden so you have to use
-- record update with 'defaultDictOptions'.
--
-- Example:
--
-- @
-- setDictOptions defaultDictOptions { autoDoc = False }
-- @
data DictOptions = DictOptions
  { -- | 'DictOptions' setting to generate a field name from the
    -- name of the class and one of its methods.
    --
    -- By default, prepend @"_"@ to alphabetic identifiers, and prepend @"|"@
    -- to operators.
    --
    -- (Note: all of these name types are synonyms for @String@.)
    methodName :: ClassName -> MethodName -> FieldName

    -- | 'DictOptions' setting to generate a field name from the name
    -- of the class and one of its superclasses. The @Int@ is a counter of duplicate
    -- superclasses, starts at 0.
    --
    -- By default, prepend @\"_\"@ to alphabetic identifiers, and prepend @\"/\"@
    -- to operators.
  , superclassName :: ClassName -> ClassName -> Int -> FieldName

    -- | 'DictOptions' setting to generate a type name from the name of the class.
    --
    -- By default, prepend @\"Dict\"@ to alphabetic identifiers, and prepend
    -- @\".\"@ to operators.
  , typeName :: ClassName -> TypeName

    -- | 'DictOptions' setting to generate a constructor name from the name of the class.
    --
    -- By default, keep alphabetic identifiers unchanged, and prepend
    -- @\":\"@ to operators.
  , constructorName :: ClassName -> ConstrName

    -- | 'DictOptions' setting to automatically generate a haddock comment:
    -- "Dictionary type for CLASS".
    --
    -- @True@ by default.
  , autoDoc :: Bool
  }

-- | Set options for subsequent invocations of 'mkDict'.
-- This setting only affects the current module.
setDictOptions :: DictOptions -> Q ()
setDictOptions = putQ

getDictOptions :: Q DictOptions
getDictOptions = fromMaybe defaultDictOptions <$> getQ

-------------------------------------------------------------------------------
-- | Creates name of field holding method implementation from method name. Name
-- is generated this way:
--
-- * Prefix names ('show', 'pure') are prefixed with @_@
-- * Operators (('<*>'), ('>>=')) are prefixed with @|@
defaultMethodName :: MethodName -> MethodName
defaultMethodName name@(c : _)
  | isAlpha c || c == '_' = "_" ++ name
  | otherwise = "|" ++ name
defaultMethodName [] = error "empty name!"

-------------------------------------------------------------------------------
-- | Creates name of field holding superclass instance from name of class. Name
-- is generated this way:
--
-- * Prefix names ('Show', 'Applicative') are prefixed with @_@
-- * Operators (('~')) are prefixed with @/@
-- * Tuples are converted into prefix names "_Tuple"
--
-- If there are multiple constraints with same name:
--
-- * Prefix names and names of tuples get numeric suffixes in order
-- * Operators are suffixed with increasing number of @|@
defaultSuperclassName :: ClassName -> Int -> MethodName
defaultSuperclassName name@(c:_) count
  | isAlpha  c = "_" ++ name     ++ index
  | c == '('   =        "_Tuple" ++ index
  | otherwise  = "/" ++ name     ++ replicate count '|'
  where
    index = if count == 0 then "" else show count
defaultSuperclassName _ _ = error "emtpy name!"

defaultTypeName :: ClassName -> TypeName
defaultTypeName name@(c : _)
  | isAlpha c = "Dict" ++ name
  | otherwise = "." ++ name
defaultTypeName [] = error "empty name!"

-------------------------------------------------------------------------------
-- | Creates name of dictionary representation data constructor from name of
-- class. Name is generated this way:
--
-- * Prefix names ('Show', 'Applicative') are kept as-is
-- * Operators (('~')) are prefixed with colon @:@
defaultConstructorName :: ClassName -> ConstrName
defaultConstructorName name@(c : _)
  | isAlpha c = name
  | otherwise = ":" ++ name
defaultConstructorName [] = error "empty name!"

-- | Default 'DictOptions'.
defaultDictOptions :: DictOptions
defaultDictOptions = DictOptions
  { methodName = \_ -> defaultMethodName
  , superclassName = \_ -> defaultSuperclassName
  , typeName = defaultTypeName
  , constructorName = defaultConstructorName
  , autoDoc = True
  }

mapName :: (String -> String) -> Name -> Name
mapName f name = mkName (f (nameBase name))

methodName' :: DictOptions -> Name -> Name -> Name
methodName' opts cname = mapName (methodName opts (nameBase cname))

superclassName' :: DictOptions -> Name -> Name -> Int -> Name
superclassName' opts cname sname i = mkName (superclassName opts (nameBase cname) (nameBase sname) i)

typeName' :: DictOptions -> Name -> Name
typeName' opts = mapName (typeName opts)

constructorName' :: DictOptions -> Name -> Name
constructorName' opts = mapName (constructorName opts)

mkDictWith :: DictOptions -> Name -> Q [Dec]
mkDictWith opts name = do
  info <- getClassDictInfo opts name
#if MIN_VERSION_template_haskell(2,18,0)
  when (autoDoc opts) $ addModFinalizer $
    putDoc (DeclDoc (typeName' opts name)) ("Dictionary type for t'" ++ pprint name ++ "'")
#endif
  pure (dictInst opts info)

-------------------------------------------------------------------------------
-- | Constructs info about class dictionary representation being created.
getClassDictInfo :: DictOptions -> Name -> Q ClassDictInfo
getClassDictInfo opts className = reify className >>= \case
  ClassI (ClassD constraints _ args _ methods) _ -> do
    let dictConName = constructorName' opts className
    pure CDI{
        className
      , dictTyArgs  = args
      , dictConName
      , dictFields  = superFieldsFromCxt opts className constraints
                   ++ mapMaybe (methodFieldFromDec opts className) methods
      }
  _ -> fail $ '\'' : nameBase className ++ "' is not a class"

#if MIN_VERSION_template_haskell(2,21,0)
type TVB = TyVarBndr BndrVis
#elif MIN_VERSION_template_haskell(2,17,0)
type TVB = TyVarBndr ()
#else
type TVB = TyVarBndr
#endif

appCon :: Name -> [TVB] -> Type
appCon cname args = foldl1' AppT $ ConT cname : map bndrToType args


-------------------------------------------------------------------------------
-- | Creates class dictionary representation fields from constraints that carry
-- runtime proof, preserving order.
superFieldsFromCxt :: DictOptions -> Name -> [Pred] -> [ClassDictField]
superFieldsFromCxt opts cname constraints = (`evalState` M.empty) do
  sequence $ mapMaybe (fmap . mkSuperField <*> appHeadName) constraints
 where
  mkSuperField c n = do
    count <- maybe 0 id . M.lookup n <$> get
    modify $ M.alter (maybe (Just 1) $ Just . (+1)) n
    pure CDF{
        fieldName   = superclassName' opts cname n count
      , fieldSource = Superclass
      , origName    = n
      , origType    = c
      }

-------------------------------------------------------------------------------
-- | Converts type variable binder to type.
#if MIN_VERSION_template_haskell(2,17,0)
bndrToType :: TyVarBndr a -> Type
bndrToType = \case
  PlainTV n _    -> VarT n
  KindedTV n _ k -> VarT n `SigT` k
#else
bndrToType :: TyVarBndr -> Type
bndrToType = \case
  PlainTV n    -> VarT n
  KindedTV n k -> VarT n `SigT` k
#endif

-------------------------------------------------------------------------------
-- | Extracts name of head of type application or returns 'Nothing'.
appHeadName :: Type -> Maybe Name
appHeadName = \case
  ForallT _ _ t      -> appHeadName t
  AppT t _           -> appHeadName t
  SigT t _           -> appHeadName t
  VarT n             -> Just n
  ConT n             -> Just n
  PromotedT n        -> Just n
  InfixT _ n _       -> Just n
  UInfixT _ n _      -> Just n
  ParensT t          -> appHeadName t
  TupleT i           -> prod "("  ',' (i - 1)  ")"
  UnboxedTupleT i    -> prod "(#" ',' (i - 1) "#)"
  UnboxedSumT i      -> prod "(#" '|' (i + 1) "#)"
  ArrowT             -> Just ''(->)
  EqualityT          -> Just ''(~)
  ListT              -> Just ''[]
  PromotedTupleT i   -> prod "(" ',' (i - 1) ")"
  PromotedNilT       -> Just '[]
  PromotedConsT      -> Just '(:)
  StarT              -> Just ''K.Type
  ConstraintT        -> Just ''K.Constraint
  LitT{}             -> Nothing
  WildCardT          -> Nothing
#if MIN_VERSION_template_haskell(2,15,0)
  AppKindT t _       -> appHeadName t
  ImplicitParamT _ t -> appHeadName t
#if MIN_VERSION_template_haskell(2,16,0)
  ForallVisT _ t        -> appHeadName t
#if MIN_VERSION_template_haskell(2,17,0)
  MulArrowT             -> Just ''(->)
#if MIN_VERSION_template_haskell(2,19,0)
  PromotedInfixT _ n _  -> Just n
  PromotedUInfixT _ n _ -> Just n
#endif
#endif
#endif
#endif
 where
  prod l d i r  = Just $ mkName if
    | i <= 0    -> l                  ++ r
    | otherwise -> l ++ replicate i d ++ r

-------------------------------------------------------------------------------
-- | Creates class dictionary representation field from class member of returns
-- 'Nothing'.
methodFieldFromDec :: DictOptions -> Name -> Dec -> Maybe ClassDictField
methodFieldFromDec opts cname = \case
#if MIN_VERSION_template_haskell(2,15,0)
  SigD n t ->
#else
  SigD n (ForallT _ _ t) ->
#endif
    Just CDF{
      fieldName   = methodName' opts cname n
    , fieldSource = Method
    , origName    = n
    , origType    = t
    }
  _ -> Nothing

-------------------------------------------------------------------------------
-- | Creates 'Dict' instance from info about class dictionary representation.
dictInst :: DictOptions -> ClassDictInfo -> [Dec]
dictInst opts cdi = [
    instDec
  , case classDictToRecField <$> dictFields cdi of
      []      -> dictDec DataD    [NormalC (dictConName cdi) []     ]
      [field] -> dictDec NewtypeD (RecC    (dictConName cdi) [field])
      fields  -> dictDec DataD    [RecC    (dictConName cdi) fields ]
  ]
 where
  dname = typeName' opts (className cdi)
#if MIN_VERSION_template_haskell(2,15,0)
  instDec            = TySynInstD $
    TySynEqn Nothing (ConT ''Dict `AppT` appCon (className cdi) (dictTyArgs cdi))
                     (appCon dname (dictTyArgs cdi))
  dictDec con fields =
    con [] dname (dictTyArgs cdi) Nothing fields []
#else
  instDec            = TySynInstD ''Dict $
      TySynEqn [appCon (className cdi) (dictTyArgs cdi)] $ appCon dname (dictTyArgs cdi)
  dictDec con fields =
    con [] dname (dictTyArgs cdi) Nothing fields []
#endif

-------------------------------------------------------------------------------
-- | Converts info about class dictionary representation field to record field.
classDictToRecField :: ClassDictField -> VarBangType
classDictToRecField cdf = (
    fieldName cdf
  , Bang NoSourceUnpackedness NoSourceStrictness
  , (case fieldSource cdf of
      Superclass -> AppT $ ConT ''Dict
      Method     -> id
    ) $ origType cdf
  )

-------------------------------------------------------------------------------
-- | Info about class dictionary used by 'mkDict'.
data ClassDictInfo = CDI{
    className   :: Name
  , dictTyArgs  :: [TVB]
  , dictConName :: Name
  , dictFields  :: [ClassDictField]
  } deriving Show

-------------------------------------------------------------------------------
-- | Info about field in class dictionary used by 'mkDict'
data ClassDictField = CDF{
    fieldName   :: Name
  , fieldSource :: ClassDictFieldSource
  , origName    :: Name
  , origType    :: Type
  } deriving Show

-------------------------------------------------------------------------------
-- | Source of field in class dictionary.
data ClassDictFieldSource = Superclass | Method deriving Show

-------------------------------------------------------------------------------
-- | Implement an instance using a dictionary. The argument must be an
-- expression quote with an explicit signature.
--
-- For example:
--
-- @
-- 'instanceDict' [| 'FCI.Base.viaFunctor' @((->) e) :: 'Dict' ('Functor' (Reader e)) |]
-- @
--
-- generates the following 'Functor' instance for a user-defined type @Reader e@:
--
-- @
-- instance 'Functor' (Reader e) where
--   -- Methods obtained from the fields of the dictionary.
--   'fmap' = 'TCI.Data.Functor._fmap' ('TCI.Data.Functor.viaFunctor' @((-> e)))
--   ('<$') = ('TCI.Data.Functor.|<$') ('TCI.Data.Functor.viaFunctor' @((-> e)))
-- @
--
-- The following forms are supported:
--
-- @
-- 'instanceDict' [| e ::                  'Dict' (C (T a)) |]
-- 'instanceDict' [| e ::           D a => 'Dict' (C (T a)) |]
-- 'instanceDict' [| e :: forall a. D a => 'Dict' (C (T a)) |]
-- @
instanceDict :: Q Exp -> Q [Dec]
instanceDict = instanceDict_ Nothing

-------------------------------------------------------------------------------
-- | Variant of 'instanceDict' with an 'Overlap' annotation
-- (@{-\# OVERLAPPABLE \#-}@, etc.).
--
-- @
-- 'instanceDict_' 'Nothing'               = 'instanceDict'
-- 'instanceDict_' ('Just' 'Overlappable')
-- 'instanceDict_' ('Just' 'Overlapping')
-- 'instanceDict_' ('Just' 'Overlaps')
-- 'instanceDict_' ('Just' 'Incoherent')
-- @
instanceDict_ :: Maybe Overlap -> Q Exp -> Q [Dec]
instanceDict_ overlap qe = do
  (className, cxt, ty, e) <- splitDictExp =<< qe
  info <- getInstanceDictInfo className ty
  fresh <- newName "field"
  pure [InstanceD overlap cxt ty (instanceDictBody (clean e) info fresh)]

data InstanceDictInfo = InstanceDictInfo
  { idiConstrName :: Name
  , idiMethodNames :: [(Name, Name)]  -- Method name, encoded field name
  }

classMethodNames :: Info -> [Name]
classMethodNames (ClassI (ClassD _ _ _ _ decs) _) = do
  SigD name _ <- decs
  pure name
classMethodNames _ = error "not a class"

dataNames :: Info -> (Name, [Name])
dataNames (TyConI (DataD _ _ _ _ [c] _)) = dataNamesCon c
dataNames (TyConI (NewtypeD _ _ _ _ c _)) = dataNamesCon c
dataNames _ = error "Not data"

dataNamesCon :: Con -> (Name, [Name])
dataNamesCon (RecC conName fields) = (conName, (\(fname, _, _) -> fname) <$> fields)
dataNamesCon (NormalC conName _) = (conName, [])
dataNamesCon _ = error "Bad data"

-- | Use the Dict instance to lookup the dictionary type associated with a type
-- class. This lets us implement the instance regardless of how the names
-- were mangled.
getInstanceDictInfo :: Name -> Type -> Q InstanceDictInfo
getInstanceDictInfo className cls = do
  instances <- reifyInstances ''Dict  [cls]
  case instances of
    TySynInstD (TySynEqn _ _ rhs) : _ | Just dataName <- appHeadName rhs -> do
      methodNames <- classMethodNames <$> reify className
      (conName, fieldNames) <- dataNames <$> reify dataName
      pure InstanceDictInfo
        { idiConstrName = conName
          -- fields also include superclasses at the beginning,
          -- so we zip from the end to drop them.
        , idiMethodNames = reverse (zip (reverse methodNames) (reverse fieldNames))
        }
    [] -> error "Dictionary not found. Did you forget to mkDict?"
    _ -> error ("Should not happen. " ++ pprint instances)

-------------------------------------------------------------------------------
-- | Extract the name of the class, the instance context, the instance head,
-- and the dictionary expression, cleaning it up a bit (remove context and @forall@).
splitDictExp :: Exp -> Q (Name, Cxt, Type, Exp)
splitDictExp e0 = do
  (cxt, t, e) <- case e0 of
    SigE e (ForallT _ cxt (AppT _Dict t)) -> pure (cxt, t, SigE e (AppT _Dict t))
    SigE _ (AppT _ t) -> pure ([], t, e0)
    _ -> badExp e0
  case appHeadName t of
    Nothing -> badExp e0
    Just n -> pure (n, cxt, t, e)

-------------------------------------------------------------------------------
-- | Error for 'instanceDict'.
badExp :: Exp -> Q a
badExp e = fail
  ("instanceDict: expected a quasiquote of the form \
[| e :: Dict (C (T a)) |] or [| e :: forall a. C0 a => Dict (C (T a)) |]\n\t\
got: [| " ++ pprint e ++ " |]")

-------------------------------------------------------------------------------
-- | Build the instance body for 'instanceDict'.
instanceDictBody :: Exp -> InstanceDictInfo -> Name -> [Dec]
instanceDictBody e info fresh = mkMethod <$> idiMethodNames info
  where
    mkMethod :: (Name, Name) -> Dec
    mkMethod (methodName, fieldName) = FunD methodName [Clause [] (NormalB (body fieldName)) []]
    body fieldName = AppE fieldLambda e
      where
        -- Field accessor expression (\(C { field = f }) -> f)
        -- With DuplicateRecordFields, we cannot use a field name @field@ as an accessor function.
        -- I could use OverloadedRecordDot but this is very new, and I'd like to keep
        -- backwards compatibility for now.
        fieldLambda = LamE [VisAP (RecP (idiConstrName info) [(fieldName, VarP fresh)])] (VarE fresh)

#if !MIN_VERSION_template_haskell(2,22,0)
pattern VisAP :: Pat -> Pat
pattern VisAP x = x
#endif

-- Work around https://gitlab.haskell.org/ghc/ghc/-/issues/14848
-- Fields declared under DuplicateRecordFields are munged and completely unusable in TH quotes.
-- So we clean them up.
clean :: Exp -> Exp
clean = everywhere (mkT cleanName)

-- "$sel:field:Constructor" -> "field"
cleanName :: Name -> Name
cleanName n | Just m <- stripPrefix "$sel:" (nameBase n) = mkName (cleann m)
  where
    cleann = reverse . drop 1 . dropWhile (/= ':') . reverse
cleanName n = n

everywhere :: (forall a. Data a => a -> a)
           -> (forall a. Data a => a -> a)
everywhere f = go
  where
    go :: forall a. Data a => a -> a
    go = f . gmapT go

mkT :: (Typeable a, Typeable b) => (b -> b) -> a -> a
mkT f = maybe id id (cast f)
