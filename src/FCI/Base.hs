{-# LANGUAGE
  AllowAmbiguousTypes,
  BlockArguments,
  CPP,
  DuplicateRecordFields,
  ImpredicativeTypes,
  KindSignatures,
  NamedFieldPuns,
  PolyKinds,
  RankNTypes,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

-- | Dictionaries for /base/ classes.

module FCI.Base (
    -- * Dictionary types
    DictEq(..)
  , DictOrd(..)
  , DictSemigroup(..)
  , DictMonoid(..)
  , DictShow(..)
  , DictRead(..)
  , DictEnum(..)
  , DictBounded(..)
  , DictNum(..)
  , DictReal(..)
  , DictIntegral(..)
  , DictFractional(..)
  , DictFloating(..)
  , DictRealFrac(..)
  , DictRealFloat(..)
  , DictBits(..)
  , DictFiniteBits(..)
  , DictIx(..)
  , DictFunctor(..)
  , DictContravariant(..)
  , DictApplicative(..)
  , DictAlternative(..)
  , DictMonad(..)
  , DictMonadFail(..)
  , DictMonadPlus(..)
  , DictMonadFix(..)
  , DictFoldable(..)
  , DictTraversable(..)
  , DictBifunctor(..)
  , DictBifoldable(..)
  , DictBitraversable(..)
  , DictException(..)
  , DictCategory(..)
  , DictArrow(..)
  , DictArrowZero(..)
  , DictArrowPlus(..)
  , DictArrowChoice(..)
  , DictArrowApply(..)
  , DictArrowLoop(..)
  , DictStorable(..)

    -- * Default dictionaries
  , fmapFunctor
  , viaFunctor
  , applyApplicative
  , liftA2Applicative
  , bindMonad
  , joinMonad
  ) where

import Control.Arrow
import Control.Applicative
import Control.Monad
import Control.Monad.Fix
import Control.Exception
import Control.Category (Category)
import Data.Bits
import Data.Coerce
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable
import Data.Functor.Contravariant
import Data.Ix
import Foreign.Storable

import FCI

mkDict ''Eq
mkDict ''Ord
mkDict ''Semigroup
mkDict ''Monoid
mkDict ''Show
mkDict ''Read
mkDict ''Enum
mkDict ''Bounded
mkDict ''Num
mkDict ''Real
mkDict ''Integral
mkDict ''Fractional
mkDict ''Floating
mkDict ''RealFrac
mkDict ''RealFloat
mkDict ''Bits
mkDict ''FiniteBits
mkDict ''Ix
mkDict ''Functor
mkDict ''Contravariant
mkDict ''Applicative
mkDict ''Alternative
mkDict ''Monad
mkDict ''MonadFail
mkDict ''MonadPlus
mkDict ''MonadFix
mkDict ''Foldable
mkDict ''Traversable
mkDict ''Bifunctor
mkDict ''Bifoldable
mkDict ''Bitraversable
mkDict ''Exception
mkDict ''Category
mkDict ''Arrow
mkDict ''ArrowZero
mkDict ''ArrowPlus
mkDict ''ArrowChoice
mkDict ''ArrowApply
mkDict ''ArrowLoop
mkDict ''Storable

-------------------------------------------------------------------------------
-- | Default t'Functor' dictionary requiring only 'fmap'.
fmapFunctor :: (forall a b. (a -> b) -> f a -> f b) -> Dict (Functor f)
fmapFunctor _fmap = Functor{
    _fmap
  , (|<$) = _fmap . const
  }

-------------------------------------------------------------------------------
-- | t'Functor' dictionary for a type coercible to another that has a t'Functor'
-- instance.
--
-- This definition is actually quite limited in applicability.
-- A better definition would use the quantified constraint
-- @(forall a. Coercible (f a) (g a))@.
viaFunctor :: forall f g. (Coercible f g, Functor f) => Dict (Functor g)
viaFunctor = Functor {
    _fmap = (coerce :: ((a -> b) -> f a -> f b) -> (a -> b) -> g a -> g b) fmap
  , (|<$) = (coerce :: (a -> f b -> f a) -> a -> g b -> g a) (<$)
  }

-------------------------------------------------------------------------------
-- | Default t'Applicative' dictionary requiring only 'pure' and @('<*>')@.
applyApplicative :: (forall a. a -> f a)                    -- ^ 'pure'
                 -> (forall a b. f (a -> b) -> f a -> f b)  -- ^ ('<*>')
                 -> Dict (Applicative f)
applyApplicative _pure (|<*>) = Applicative{
    _Functor = fmapFunctor $ (|<*>) . _pure
  , _pure
  , (|<*>)
  , _liftA2  = \f fa fb -> _pure f |<*> fa |<*> fb
  , (|*>)    = \fa fb -> _pure (const id) |<*> fa |<*> fb
  , (|<*)    = \fa fb -> _pure const |<*> fa |<*> fb
  }

-------------------------------------------------------------------------------
-- | Default t'Applicative' dictionary requiring only 'pure' and 'liftA2'.
liftA2Applicative :: (forall a. a -> f a)
                  -- ^ 'pure'
                  -> (forall a b c. (a -> b -> c) -> f a -> f b -> f c)
                  -- ^ 'Control.Applicative.liftA2'
                  -> Dict (Applicative f)
liftA2Applicative _pure _liftA2 = Applicative{
    _Functor = fmapFunctor $ ($ _pure ()) . _liftA2 . const
  , _pure
  , (|<*>)   = _liftA2 ($)
  , _liftA2
  , (|*>)    = _liftA2 $ const id
  , (|<*)    = _liftA2 const
  }

-------------------------------------------------------------------------------
-- | Default t'Monad' dictionary requiring only 'return' and @('>>=')@.
bindMonad :: (forall a. a -> m a)                    -- ^ 'return'
          -> (forall a b. m a -> (a -> m b) -> m b)  -- ^ ('>>=')
          -> Dict (Monad m)
bindMonad _return (|>>=) = Monad{
    _Applicative = applyApplicative _return \mf ma ->
                     mf |>>= \f -> ma |>>= \a -> _return $ f a
  , (|>>=)
  , (|>>)        = \ma -> (ma |>>=) . const
  , _return
#if !MIN_VERSION_GLASGOW_HASKELL(8,8,0,0)
  , _fail        = error
#endif
  }

-------------------------------------------------------------------------------
-- | Default t'Monad' dictionary requiring only 'fmap', 'return', and 'Control.Monad.join'.
joinMonad :: (forall a b. (a -> b) -> m a -> m b)  -- ^ 'fmap'
          -> (forall a. a -> m a)                  -- ^ 'return'
          -> (forall a. m (m a) -> m a)            -- ^ 'Control.Monad.join'
          -> Dict (Monad m)
joinMonad _fmap _return _join = Monad{
    _Applicative = applyApplicative _return \mf ma ->
                     _join $ _fmap (`_fmap` ma) mf
  , (|>>=)       = \ma -> _join . flip _fmap ma
  , (|>>)        = \ma -> _join . flip _fmap ma . const
  , _return
#if !MIN_VERSION_GLASGOW_HASKELL(8,8,0,0)
  , _fail        = error
#endif
  }
